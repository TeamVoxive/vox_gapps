Vox GApps [DISCONTINUED]
============
This project has been discontinued, you are free to use the code as you please!

Alternative: [Open GApps](http://opengapps.github.io/opengapps/)
------------
I have joined the Open GApps team to build a customizable and user-powered GApps!

- More customization and up-to-date
    - Set up your .gapps-config file to get the same features!
    - If you still want AROMA, check out the `aroma` variant!
    - Also, now you can update Factory Images with the `fornexus` variant!
- You can actually build your own version with the script!
- There based off of a similar GApps so the feel will be the same!