#!/sbin/sh
# GApps Install Preparation Script

echo -n -e "# begin Voxive GApps properties\n# This file contains information needed to flash Voxive GApps\n"  > /tmp/info.prop

# Does device qualify for FaceUnlock?
good_ffc_device() {
  if [ -f /sdcard/Voxive/.forcefaceunlock ]; then
    return 0
  fi
  if cat /proc/cpuinfo |grep -q Victory; then
    return 1
  fi
  if cat /proc/cpuinfo |grep -q herring; then
    return 0
  fi
  if cat /proc/cpuinfo |grep -q Victory; then
    return 1
  fi
  if cat /proc/cpuinfo |grep -q herring; then
    return 1
  fi
  if cat /proc/cpuinfo |grep -q sun4i; then
    return 1
  fi
  return 0
}

# Final determination to decide if FaceUnlock should be installed
if good_ffc_device && [ -e /system/etc/permissions/android.hardware.camera.front.xml ]; then
    echo -e "\ninstall.faceunlock=true" >> /tmp/info.prop
else
    echo -e "\ninstall.faceunlock=false" >> /tmp/info.prop
fi

# Here we'll determine if and what type of photosphere device camera/gallery to install
# .forcenophotosphere will take precedence over .forcephotosphere
if [ -e /sdcard/.forcenophotosphere ]; then
    inst_ps=false
    ps_dev=none
elif (grep -qi "hammerhead" /proc/cpuinfo /sdcard/.forcephotosphere ); then
    inst_ps=true
    ps_dev=hammerhead
elif (grep -qi "flo" /proc/cpuinfo /sdcard/.forcephotosphere ); then
    inst_ps=true
    ps_dev=flo
elif (grep -qi "deb" /proc/cpuinfo /sdcard/.forcephotosphere ); then
    inst_ps=true
    ps_dev=deb
elif (grep -qi "mako" /proc/cpuinfo /sdcard/.forcephotosphere ); then
    inst_ps=true
    ps_dev=mako
elif (grep -qi "manta" /proc/cpuinfo /sdcard/.forcephotosphere ); then
    inst_ps=true
    ps_dev=manta
elif (grep -qi "tuna" /proc/cpuinfo /sdcard/.forcephotosphere ); then
    inst_ps=true
    ps_dev=tuna
else
    inst_ps=false
    ps_dev=none
fi

echo "install.faceunlock=$inst_fu" >> /tmp/gapps.prop
echo "install.photosphere=$inst_ps" >> /tmp/gapps.prop
echo "photosphere.device=$ps_dev" >> /tmp/gapps.prop
echo "# end Voxive GApps properties" >> /tmp/info.prop

# Copy info.prop to Famigo folder on SD Card
cp -af /tmp/info.prop /sdcard/Voxive/logs/info.prop
