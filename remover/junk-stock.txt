# AOSP
#Browser
#Calculator
Calendar*
#ChromeBookmarksS*      # Chrome Browser Bookmarks Sync
#DeskClock
Development             # Development Tools
#Email*
#Exchange*              # Exchange for Email
Gallery*
GenieWidget             # News and Weather
#LatinIME
Music
QuickSearchBox
SoundRecorder
SpareParts
Term                    # Android Terminal Emulator
Torch                   # Flashlight
VideoEditor
VoiceDialer

# AOSPA
HALO                     # Add Shortcuts to HALO
Lightbulb                # Torch

# CM
Apollo                   # Music Player
CMFileManager
DSPManager               # Music Effects
LockClock                # Clock Widget
Trebuchet                # Launcher for CyanogenMod-based ROMs
VoicePlus                # Google Voice SMS
CMWallpapers
WhisperPush              # Secure SMS

# DAYDREAMS
BasicDreams
PhotoTable

# KEYBOARDS
iWnnIME*
KoreanIME
OpenWnn
*PinyinIME
XT9IME

# WALLPAPERS
#LiveWallpapersPicker     # Picker
LiveWallpapers
BlueBalls
Galaxy4
HoloSpiral*
MagicSmoke*
NoiseField
PhaseBeam
SunBeam
Visualization*

# PAC
GooglePacman             # Google.com Pac-Man Game
#PacConsole              # Updater and Changelog
PacPapers                # Wallpapers
#PacStats                # ROM Stats

# SLIM
SlimFileManager
SlimIRC

# OMNI
OmniSwitch              # Multitasking Replacement
DashClock
MonthCalendarWidget

# CARBON
CarbonAbout             # About Screen
#ROMStats               # ROM Stats
Wallpapers

# AOSB (ProBAM)
#AosbOTA                # OTA Updater
appsetting              # Xposed App Settings
xposed_installer        # Xposed
